output "dns_zone" {
  value = join(".", slice(split(".", var.app_fqdn), length(split(".", var.app_fqdn)) - 2, length(split(".", var.app_fqdn))))
}

output "dns_subdomains" {
  value = join(".", slice(split(".", var.app_fqdn), 0, length(split(".", var.app_fqdn)) - 2))
}

output "ecs_vpc" {
  value = data.aws_vpc.ecs_vpc.id
}

output "app_fqdn" {
  value = var.app_fqdn
}

resource "ovh_domain_zone_record" "service_record" {
  zone      = join(".", slice(split(".", var.app_fqdn), length(split(".", var.app_fqdn)) - 2, length(split(".", var.app_fqdn))))
  subdomain = join(".", slice(split(".", var.app_fqdn), 0, length(split(".", var.app_fqdn)) - 2))
  fieldtype = "CNAME"
  ttl       = 60
  target    = "${data.aws_cloudfront_distribution.entrypoint.domain_name}."
}

resource "ovh_domain_zone_record" "www_service_record" {
  zone      = join(".", slice(split(".", var.app_fqdn), length(split(".", var.app_fqdn)) - 2, length(split(".", var.app_fqdn))))
  subdomain = "www.${join(".", slice(split(".", var.app_fqdn), 0, length(split(".", var.app_fqdn)) - 2))}"
  fieldtype = "CNAME"
  ttl       = 60
  target    = "${data.aws_cloudfront_distribution.entrypoint.domain_name}."
}
